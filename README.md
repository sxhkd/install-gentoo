# Install Gentoo!
* In gentoo live minimal iso  
`$ git clone https://gitlab.com/sxhkd/install-gentoo.git`  
`# chmod +x install-gentoo`  
`# ./install-gentoo`

## What this project does:
* Installs gentoo
  - Little to no need for user intervention, e.g. reboot, or dump to chroot shell
  - Uses genkernel as well as grub (make your own kernel! Or use my .config)
  - Tool to be able end automatic install at a given checkpoint, e.g. dump user into first chroot
  - Standard gentoo minimal-openrc stage3 install
  - configures `make.conf` and `make.conf.lto` so you don't have to
* Install needed programs
  - Installs `mv ltoize librewolf guru` repositories
  - Automatically install a few repositories used by the author
  - Optimization to the max (-Ofast, pgo, graphite, clang and ninja, and ltoize repository)
* Hopefully minimizes dependency hell, trust me, it isn't fun, especially for the hundredth install

## Does ***not***:
* Patch ebuilds (no reason to in my opinion)
* Overide compiler judgement (don't do this, I beg of you.)
* Use the ~amd64 keywork package list, had far too many issues with packages being no longer be able to compile after a quick `emaint sync --auto`
  - As well as entering into the inner circles of dependency hell

## Why:
* Had enough manually installing gentoo?
* Don't feel like constantly distrohopping (and eventually returning to gentoo)?
* This project makes a gentoo install that's at least somewhat reproducable.
* Automatically configures portage to be optimized for optimization

## notes:
* this project assumes decent understanding of linux and gentoo
* this project is not intended for beginners nor people looking for flushed out desktop
* trouble with JIT compiled programs, namely lua

## Installs:
* gentoo base install
* ltoize
* emacs
* neovim
* mpv 
* mpd
* nsxiv
* lf
* librewolf
* video and audio codecs
* image libraries
* ueberzug
* pipewire with pulseaudio compatability
* network manager
* genkernel with gentoo sources
* grub
* bluetoothctl
* zsh
* zsh-syntax-highlighting
* mutt-wizard
* portage utilities, e.g. `emaint, eix...`
* xorg as well as correct gpu drivers

## Does not include:
* s(o)ystemD, this is a joke, also I just don't have enough of a reason to justify the overhead or time to configure it
* desktop environments, or any massive graphics environments as I just don't use them
* any kernel configuration other than genkernel, use kernel seed or make your own
* dwm, st, or dmenu as those can be found in another project
* complete package list because I don't feel like listing out entire gentoo base install
